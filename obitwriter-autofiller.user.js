// ==UserScript==
// @name         Obitwriter Autofiller
// @version      1.1
// @description  Autofill obituary form
// @author       Andrzej Sliwinski <andrzej.sliwinski@gmail.com>
// @updateURL    https://bitbucket.org/andrzej-sliwinski/obitwriter-autofiller/raw/master/obitwriter-autofiller.js
// @match        https://www.legacy.local/obitwriter/identity*
// @match        https://www.dev-legacy.com/obitwriter/identity*
// @match        https://www.qa-legacy.com/obitwriter/identity*
// @match        https://www.legacy.com/obitwriter/identity*
// @grant        none
// ==/UserScript==

(function() {
  'use strict';

  const states = {
    Alabama: 'AL',
    Montana: 'MT',
    Alaska: 'AK',
    Nebraska: 'NE',
    Arizona: 'AZ',
    Nevada: 'NV',
    Arkansas: 'AR',
    'New Hampshire': 'NH',
    California: 'CA',
    'New Jersey': 'NJ',
    Colorado: 'CO',
    'New Mexico': 'NM',
    Connecticut: 'CT',
    'New York': 'NY',
    Delaware: 'DE',
    'North Carolina': 'NC',
    Florida: 'FL',
    'North Dakota': 'ND',
    Georgia: 'GA',
    Ohio: 'OH',
    Hawaii: 'HI',
    Oklahoma: 'OK',
    Idaho: 'ID',
    Oregon: 'OR',
    Illinois: 'IL',
    Pennsylvania: 'PA',
    Indiana: 'IN',
    'Rhode Island': 'RI',
    Iowa: 'IA',
    'South Carolina': 'SC',
    Kansas: 'KS',
    'South Dakota': 'SD',
    Kentucky: 'KY',
    Tennessee: 'TN',
    Louisiana: 'LA',
    Texas: 'TX',
    Maine: 'ME',
    Utah: 'UT',
    Maryland: 'MD',
    Vermont: 'VT',
    Massachusetts: 'MA',
    Virginia: 'VA',
    Michigan: 'MI',
    Washington: 'WA',
    Minnesota: 'MN',
    'West Virginia': 'WV',
    Mississippi: 'MS',
    Wisconsin: 'WI',
    Missouri: 'MO',
    Wyoming: 'WY'
  };

  const services = [
    'Visitation',
    'Funeral service',
    'Memorial service',
    'Celebration of Life',
    'Burial',
    'Graveside service',
    'Wake',
    'Lying in State',
    'Rosary',
  ];

  Date.prototype.addDays = function(days) {
    var date = new Date(this.valueOf());
    date.setDate(date.getDate() + days);
    return date;
  };

  function setNativeValue(element, value) {
    const { set: valueSetter } = Object.getOwnPropertyDescriptor(element, 'value') || {}
    const prototype = Object.getPrototypeOf(element)
    const { set: prototypeValueSetter } = Object.getOwnPropertyDescriptor(prototype, 'value') || {}

    if (valueSetter && valueSetter !== prototypeValueSetter) {
      prototypeValueSetter.call(element, value);
    } else {
      valueSetter.call(element, value);
    }

    element.dispatchEvent(new Event('input', { bubbles: true }));
  }

  function reactJSSetValue(elm, value) {
    elm.value = value;
    elm.defaultValue = value;
    elm.dispatchEvent(new Event('change', { bubbles: true }));
  }

  function getRandomDate(from, to) {
    if (!from) {
      from = new Date(1900, 0, 1).getTime();
    } else {
      from = from.getTime();
    }
    if (!to) {
      to = new Date(2100, 0, 1).getTime();
    } else {
      to = to.getTime();
    }
    return new Date(from + Math.random() * (to - from));
  }

  function calculateAge(birthday, deathday) { // birthday is a date
    var ageDifMs = deathday - birthday.getTime();
    var ageDate = new Date(ageDifMs); // miliseconds from epoch
    return Math.abs(ageDate.getUTCFullYear() - 1970);
  }

  function capitalize(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
  }

  function addButton() {
    const element = document.createElement("button");
    element.type = 'button';
    element.innerHTML = 'Fill';
    const styles = [
      'font-size: 14px;',
      'position: fixed;',
      'bottom: 40px;',
      'z-index: 999;',
      'right: 20px;',
      'background-color: #E6C175;',
      'border: 0;',
      'color: #222;',
      'border-radius: 3px;',
      'padding: 1em 2em;',
      'min-width: 180px;',
      'text-align: center;',
      'cursor: pointer;'
    ].join('');
    element.setAttribute('style', styles);
    element.onclick = function() {
      main();
    };

    const body = document.getElementsByTagName("body")[0];
    body.appendChild(element);
    return element;
  }

  const button = addButton();

  async function main() {
    button.innerHTML = 'Loading ...';

    const pick = Math.floor( Math.random() * 3 );

    let gender, url;

    switch (pick) {
      case 0:
        gender = 'M'
        url = 'https://randomuser.me/api/?nat=us&results=2&gender=male'
        break;
      case 1:
        gender = 'F'
        url = 'https://randomuser.me/api/?nat=us&results=2&gender=female'
        break;
      default:
        gender = 'N'
        url = 'https://randomuser.me/api/?nat=us&results=2'
        break;
    }

    const fetchFakeName = fetch(url)
      .then(res => res.json());

    const fetchCities = fetch('https://corsp.herokuapp.com/https://www.getrandomthings.com/data/us-cities.php', {method: "POST", headers: {
        "Content-Type": "application/x-www-form-urlencoded",
      }, body: 'num=3&add=address&unique=false'})
      .then(res => res.text())
      .then(text => new DOMParser().parseFromString(text, 'text/html'));

    const [fakeName, documentCities] = await Promise.all([fetchFakeName, fetchCities]);

    const fakeNameObj = fakeName.results[0];
    const fakeNameObj2 = fakeName.results[1];

    const fName = capitalize(fakeNameObj.name.first);
    const mName = capitalize(fakeNameObj2.name.first);
    const lName = capitalize(fakeNameObj.name.last);
    const maidName = capitalize(fakeNameObj2.name.last);
    const nName = fakeNameObj.login.username;
    const bDate = new Date(fakeNameObj.dob.date);
    const bMonth = bDate.getMonth();
    const bDay = bDate.getDate();
    const bYear = bDate.getFullYear();

    const firstName = document.querySelector("input[name='identity.name.first']");
    const middleName = document.querySelector("input[name='identity.name.middle']");
    const lastName = document.querySelector("input[name='identity.name.last']");
    const maidenName = document.querySelector("input[name='identity.name.maiden']");
    const nickName = document.querySelector("input[name='identity.name.nickname']");
    const prefix = document.querySelector("select[name='identity.name.prefix']");
    const suffix = document.querySelector("select[name='identity.name.suffix']");
    const pronounMale = document.getElementById("Male");
    const pronounFemale = document.getElementById("Female");
    const pronounNeutral = document.getElementById("Neutral");
    const birthMonth = document.querySelector("select[name='identity.birthInformation.date.month']");
    const birthDay = document.querySelector("input[name='identity.birthInformation.date.day']");
    const birthYear = document.querySelector("input[name='identity.birthInformation.date.year']");

    const birthCity = document.querySelector("input[name='identity.birthInformation.location.city']");
    const birthState = document.querySelector("select[name='identity.birthInformation.location.stateProvince']");
    const birthCountry = document.querySelector("input[name='identity.birthInformation.location.country']");

    const deathMonth = document.querySelector("select[name='identity.deathInformation.date.month']");
    const deathDay = document.querySelector("input[name='identity.deathInformation.date.day']");
    const deathYear = document.querySelector("input[name='identity.deathInformation.date.year']");
    const age = document.querySelector("input[name='identity.age']");

    const deathCity = document.querySelector("input[name='identity.deathInformation.location.city']");
    const deathState = document.querySelector("select[name='identity.deathInformation.location.stateProvince']");
    const deathCountry = document.querySelector("input[name='identity.deathInformation.location.country']");

    const homeCity = document.querySelector("input[name='identity.hometown.location.city']");
    const homeState = document.querySelector("select[name='identity.hometown.location.stateProvince']");
    const homeCountry = document.querySelector("input[name='identity.hometown.location.country']");

    const service = document.querySelector("select[name='services[0].type']");
    const serviceMonth = document.querySelector("select[name='services[0].date.month']");
    const serviceDay = document.querySelector("input[name='services[0].date.day']");
    const serviceYear = document.querySelector("input[name='services[0].date.year']");

    const privateService = document.getElementById("services[0].isPrivate");
    const serviceAtFuneralHome = document.getElementById("services[0].serviceAtFuneralHome");

    setNativeValue(firstName, fName);
    setNativeValue(middleName, mName);
    setNativeValue(lastName, lName);
    setNativeValue(maidenName, maidName);

    setNativeValue(nickName, nName);

    const malePronouns = ['Mr.', 'Dr.', 'Fr.', 'Br.', 'Pr.', 'Rev.', 'Rabbi', 'Elder'];
    const femalePronouns = ['Miss', 'Mrs.', 'Ms.', 'Dr.', 'Sr.'];
    const neutralPronouns = ['Mx.', 'Dr.'];

    const maleSuffixes = ['Sr.', 'Jr.', 'PhD', 'M.D.', 'II', 'III', 'IV'];
    const femaleSuffixes = ['PhD', 'M.D.'];
    const neutralSuffixes = ['PhD', 'M.D.', 'II', 'III', 'IV'];

    const arrayRandom = (myArray) => Math.floor(Math.random() * myArray.length);

    reactJSSetValue(prefix, gender === 'M' ? malePronouns[arrayRandom(malePronouns)] : gender === 'F' ? femalePronouns[arrayRandom(femalePronouns)] : neutralPronouns[arrayRandom(neutralPronouns)]);

    reactJSSetValue(suffix, gender === 'M' ? maleSuffixes[arrayRandom(maleSuffixes)] : gender === 'F' ? femaleSuffixes[arrayRandom(femaleSuffixes)] : neutralSuffixes[arrayRandom(neutralSuffixes)]);

    gender === 'M' ? pronounMale.click() : gender === 'F' ? pronounFemale.click() : pronounNeutral.click();

    reactJSSetValue(birthMonth, bMonth);
    setNativeValue(birthDay, bDay);
    setNativeValue(birthYear, bYear);

    setNativeValue(birthCity, documentCities.querySelector("div > div:nth-child(1)").childNodes[0].data.trim());

    const stBirth = documentCities.querySelector("div > div:nth-child(1) > small").innerHTML;

    reactJSSetValue(birthState, states[stBirth]);

    const deathDate = getRandomDate(new Date(bYear, bMonth, bDay), new Date());

    reactJSSetValue(deathMonth, deathDate.getMonth());
    setNativeValue(deathDay, deathDate.getDate());
    setNativeValue(deathYear, deathDate.getFullYear());
    setNativeValue(age, calculateAge(new Date(bYear, bMonth, bDay), new Date(deathDate.getFullYear(), deathDate.getMonth(), deathDate.getDate())));

    setNativeValue(deathCity, documentCities.querySelector("div > div:nth-child(2)").childNodes[0].data.trim());

    const stDeath = documentCities.querySelector("div > div:nth-child(2) > small").innerHTML;

    reactJSSetValue(deathState, states[stDeath]);

    setNativeValue(homeCity, documentCities.querySelector("div > div:nth-child(3)").childNodes[0].data.trim());

    const stHome = documentCities.querySelector("div > div:nth-child(3) > small").innerHTML;

    reactJSSetValue(homeState, states[stHome]);

    const serviceDate = getRandomDate(new Date(), (new Date()).addDays(30));
    reactJSSetValue(service, services[arrayRandom(services)]);
    reactJSSetValue(serviceMonth, serviceDate.getMonth());
    setNativeValue(serviceDay, serviceDate.getDate());
    setNativeValue(serviceYear, serviceDate.getFullYear());

    const binaryArray = [0, 1];

    arrayRandom(binaryArray) === 0 ? privateService.click() : null;
    arrayRandom(binaryArray) === 0 ? serviceAtFuneralHome.click() : null;

    button.innerHTML = 'Fill';

  }

})();